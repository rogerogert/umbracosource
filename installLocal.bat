REM Instalacao das tabelas do Umbraco no banco configurado no web.config
bin\Chauffeur.Runner.exe install Y

REM Alteracao da senha do usuario Admin 
bin\Chauffeur.Runner.exe user change-password admin 1234567890

@ECHO:
@ECHO:
@ECHO:
@ECHO -------------------------------------------------------
@ECHO:
@ECHO Instalacao Finalizada
@ECHO:
@ECHO Revise comandos executados no console
@ECHO:
@ECHO -------------------------------------------------------
@ECHO:
@ECHO:
PAUSE